const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */
exports.lambdaHandler = async (event, context) => {
    try {
        // const ret = await axios(url);
        response = {
            'statusCode': 200,
            'body': JSON.stringify({
                message: 'hello twitch world',
                // location: ret.data.trim()
            })
        }
    } catch (err) {
        console.log(err);
        return err;
    }

    return response
};

exports.pipelineStatus = async (event, context) => {
    const projectId = '18598192'
    const ret = await axios(`https://gitlab.com/api/v4/projects/${projectId}/pipelines?order_by=updated_at`);
    return {
        'statusCode': 200,
        'body': JSON.stringify(ret.data),
    };
}

async function getStatus(event, context) {
    const projectId = '18598192'
    const ret = await axios(`https://gitlab.com/api/v4/projects/${projectId}/pipelines?order_by=updated_at`);
    console.log(ret);
    return new Promise((resolve, reject) => {
        resolve({ foo: 'bar' })
    })
}